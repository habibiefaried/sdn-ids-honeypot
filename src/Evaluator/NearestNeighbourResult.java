/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluator;

import java.util.Comparator;

/**
 *
 * @author LightMind
 */
public class NearestNeighbourResult {
    public int skor;
    public String verdict;
    public String ip_src;
    public String ip_dst;
    public int port_src;
    public int port_dst;
    
    @Override
    public String toString(){
        return ip_src+" "+ip_dst+" "+port_src+" "+port_dst+" "+verdict+" "+skor; 
    }
    
    public static Comparator<NearestNeighbourResult> skorComp = (NearestNeighbourResult t, NearestNeighbourResult t1) -> t1.skor - t.skor;
    
}
