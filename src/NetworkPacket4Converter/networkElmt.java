/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkPacket4Converter;

import java.util.ArrayList;

/**
 *
 * @author LightMind
 */
public class networkElmt {
    public int FrameNumber;
    /** Layer 2 **/
    public String eth_src; //MAC Address asal
    public String eth_dst; //MAC Address tujuan
    
    /** Layer 3 **/
    public String ip_src; //IP Address asal
    public String ip_dst; //IP Address tujuan
    public int TTL; //time to leave packet
    
    public String tipe_paket; //UDP, TCP, ICMP?
    
    /** Layer 4 **/
    public int port_src; //Port asal (bisa berupa nama service)
    public int port_dst; //Port tujuan (bisa berupa nama service)
    
    public int type; //ICMP type
    public int code; //ICMP code
    
    public ArrayList<String> tcp_flags = new ArrayList<String>();
    public int jumlahPaket = 1;
    
    public String timestamp; //waktu paket dalam date
    public long time_packet;
    
    public String payload_str; //string payload
    public int payload_length; //panjang payload
    
    public String label_attack; //label
    
    //public String hexdump;
    
    @Override
    public String toString(){
        String output = "";
        
        output += "===========================================================\n";
        output += "INFO PAKET\n";
        output += "===========================================================\n";
        output += "Frame Number: "+FrameNumber+"\n";
        output += "Timestamp: "+timestamp+"\n"; 
        //output += "Hexdump: "+hexdump+"\n";
        
        output += "Nanotime: "+time_packet+"\n";
        output += "MAC Source: "+eth_src+"\n";
        output += "MAC Destination: "+eth_dst+"\n";
        output += "IP Source: "+ip_src+"\n";
        output += "IP Destination: "+ip_dst+"\n";
        output += "Time to Leave: "+TTL+"\n";
        output += "Tipe Paket: "+tipe_paket+"\n";
        output += "Port Source: "+port_src+"\n";
        output += "Port Destination: "+port_dst+"\n";
        output += "ICMP type: "+type+"\n";
        output += "ICMP code: "+code+"\n";
        output += "Payload Length: "+payload_length+"\n";
        output += "Label Attack: "+label_attack+"\n";
        for (String tcp_flag : tcp_flags) {
            output += "FLAGS: " + tcp_flag + "\n";
        }  
        output += "\n"; 
        return output;
    }
}
